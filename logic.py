from typing import List
import datetime
import db
import model

TITLE_LIMIT = 30
TEXT_LIMIT = 200

class LogicException(Exception):
    pass

class NoteLogic:
    def __init__(self):
        self._note_db = db.NoteDB()

    def _validate_note(self, note: model.Note, existing_notes: List[model.Note]):
        if note is None:
            raise LogicException("note is None")
        if note.title is None or len(note.title) > TITLE_LIMIT:
            raise LogicException(f"title length > MAX: {TITLE_LIMIT}")
        if note.text is None or len(note.text) > TEXT_LIMIT:
            raise LogicException(f"text length > MAX: {TEXT_LIMIT}")
        if note.date is None:
            raise LogicException("date is None")

        note_date = note.date
        for existing_note in existing_notes:
            if existing_note.date == note_date:
                raise LogicException("Only one event per day is allowed")

    def create(self, note: model.Note) -> str:
        existing_notes = self._note_db.list()
        self._validate_note(note, existing_notes)
        try:
            return self._note_db.create(note)
        except Exception as ex:
            raise LogicException(f"failed CREATE operation with: {ex}")

    def list(self) -> List[model.Note]:
        try:
            return self._note_db.list()
        except Exception as ex:
            raise LogicException(f"failed LIST operation with: {ex}")

    def read(self, _id: str) -> model.Note:
        try:
            return self._note_db.read(_id)
        except Exception as ex:
            raise LogicException(f"failed READ operation with: {ex}")

    def update(self, _id: str, note: model.Note):
        existing_notes = self._note_db.list()
        for existing_note in existing_notes:
            if existing_note.id == _id:
                old_note = existing_note
                break

        if old_note.date != note.date:
            try:
                return self._note_db.update(_id, note)
            except Exception as ex:
                raise LogicException(f"failed UPDATE operation with: {ex}")
        else:
            try:
                notes_on_date = [existing_note for existing_note in existing_notes if
                                 existing_note.date == old_note.date]
                if len(notes_on_date) == 1 or note.id == _id:
                    return self._note_db.update(_id, note)
                else:
                    raise LogicException("Only one event per day is allowed")
            except Exception as ex:
                raise LogicException(f"failed UPDATE operation with: {ex}")

    def delete(self, _id: str):
        try:
            return self._note_db.delete(_id)
        except Exception as ex:
            raise LogicException(f"failed DELETE operation with: {ex}")
